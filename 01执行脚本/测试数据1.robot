*** Settings ***
Library   custom.frame.Ihxtentrance.LhxtIntrance

*** Variables ***
${data_path}   ${EXECDIR}\\02测试数据\\测试数据1.xls

*** Test Case ***
套件执行前预处理
   blanking_loc

request_打开请求测试页面
   ${sheet_name}   Set Variable   request
   ${judgment}   in_teardown   # 此方法来源:修改robot的BuiltIn.py源码
   log   ${judgment.statistics.all.failed}
   Run Keyword If   ${judgment.statistics.all.failed}>1   wei_raise_error   前置步骤执行失败,框架主动终止用例执行
   main   ${data_path}   ${sheet_name}

request1_发起请求1
   ${sheet_name}   Set Variable   request1
   ${judgment}   in_teardown   # 此方法来源:修改robot的BuiltIn.py源码
   log   ${judgment.statistics.all.failed}
   Run Keyword If   ${judgment.statistics.all.failed}>1   wei_raise_error   前置步骤执行失败,框架主动终止用例执行
   main   ${data_path}   ${sheet_name}

request2_发起请求2
   ${sheet_name}   Set Variable   request2
   ${judgment}   in_teardown   # 此方法来源:修改robot的BuiltIn.py源码
   log   ${judgment.statistics.all.failed}
   Run Keyword If   ${judgment.statistics.all.failed}>1   wei_raise_error   前置步骤执行失败,框架主动终止用例执行
   main   ${data_path}   ${sheet_name}

request3_发起请求3
   ${sheet_name}   Set Variable   request3
   ${judgment}   in_teardown   # 此方法来源:修改robot的BuiltIn.py源码
   log   ${judgment.statistics.all.failed}
   Run Keyword If   ${judgment.statistics.all.failed}>1   wei_raise_error   前置步骤执行失败,框架主动终止用例执行
   main   ${data_path}   ${sheet_name}

