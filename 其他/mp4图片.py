import cv2
import os
 
cap = cv2.VideoCapture('VID_20201028_081707.mp4')
FPS = cap.get(5)
c = 1
# timeRate = 10  # 截取视频帧的时间间隔（这里是每隔10秒截取一帧）
st = '1.11' #开始时间，等同于25分15秒
ed = '1.35' #结束时间，等同于26分35秒
start = int(st.split('.')[0]) * 60 + int(st.split('.')[1])
end = int(ed.split('.')[0]) * 60 + int(ed.split('.')[1])
startframe = int(FPS) * start  # 因为cap.get(5)获取的帧数不是整数，所以需要取整一下（向下取整用int，四舍五入用round，向上取整需要用math模块的ceil()方法）
endframe = int(FPS) * end
output = r'F:\xxx\output\long_{}-{}'.format(st,ed)
if not os.path.exists(output):
    os.makedirs(output)
 
while True:
    ret, frame = cap.read()
    if ret:
        if startframe<= c <= endframe and c%30==0:   #我自己自定的是每隔100帧保存一次
            print("开始截取视频第：" + str(c) + " 帧")
            # 这里就可以做一些操作了：显示截取的帧图片、保存截取帧到本地
            # print(frame.shape)
            cv2.imwrite(output + r'\\' + str(c) + '.jpg', frame)  # 这里是将截取的图像保存在本地
        # cv2.waitKey(0)
        if c>endframe:
            print('需要的帧已截取')
            break
        c += 1
    else:
        print("所有帧都已经保存完成")
        break
