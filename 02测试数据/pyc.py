import py_compile
import os


def dir_pyc(path):
    e = os.walk(path)
    for i in e:
        for j in i[2]:
            if j[-3] == '.py':
                p1 = i[0].replace(path, './Custom/') + f'/{j}c'
                py_compile.compile(f'{i[0]}\\{j}', p1)


def file_pyc(path):
    if path[-3:] == '.py':
        py_compile.compile(path)


def compile_py(path):
    if os.path.isdir(path):
        dir_pyc(path)
    elif os.path.isfile(path):
        file_pyc(path)
