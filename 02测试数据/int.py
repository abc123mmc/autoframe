import os
import sys
import shutil


python_path = [i for i in sys.path if i[-18] == '\\lib\\site-packages'][0]
shutil.copy('../03关键字/BuiltIn.py', python_path + '\\robot\\libraries\\BuiltIn.py')
custom_path = python_path + '\\Custom'
if os.path.exists(custom_path):
    shutil.rmtree(custom_path)
shutil.copytree('../03关键字/Custom', custom_path)
